#!/usr/bin/env bash
docker build -t ecrsss/kbadm:debug -f remoteDebug.Dockerfile .
docker push ecrsss/kbadm:debug
kubectl delete pod kbadm-pod-rd -n kbadm-space
kubectl apply -f ./k8s/kbadmpod-remote-debug.yml