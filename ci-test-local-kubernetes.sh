#!/usr/bin/env bash
docker build -t ecrsss/kbadm .
docker push ecrsss/kbadm:latest
kubectl delete pod kbadm-pod -n kbadm-space
kubectl apply -f ./k8s/kbadmpod.yml