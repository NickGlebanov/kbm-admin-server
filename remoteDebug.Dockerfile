FROM gradle:jdk-alpine
MAINTAINER n.n.glebanov@gmail.com
EXPOSE 8044 5005
COPY . /k8admserver
WORKDIR /k8admserver
ENTRYPOINT ["./gradlew","bootRun", "--debug-jvm"]