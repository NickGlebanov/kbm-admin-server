package com.mtsb.k8sadmserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sadmserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(K8sadmserverApplication.class, args);
	}

}

