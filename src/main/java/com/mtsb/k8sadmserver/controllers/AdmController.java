package com.mtsb.k8sadmserver.controllers;

import com.mtsb.k8sadmserver.kuber.KuberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdmController {

    Logger logger = LoggerFactory.getLogger(AdmController.class);

    @Autowired
    private KuberService kuberService;

    @Value("${kbadm.monolith.namespace}")
    private String appNamespace;

    @Value("${kbadm.monolith.name}")
    private String appName;

    @GetMapping("/")
    public String viewHomePage(Model model) {
        model.addAttribute("depstatus",kuberService.getPodStatus(appNamespace, appName));
        return "admpage";
    }

    @GetMapping("/restart")
    public String restartPod(Model model) {
        //todo kuberService.deploymentRolloutRestart()
        model.addAttribute("depstatus",kuberService.getPodStatus(appNamespace, appName));
        return "redirect:/";
    }
}
