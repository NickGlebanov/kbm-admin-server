package com.mtsb.k8sadmserver.kuber;

import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class KuberService {


    private KubernetesClient k8s = new KubernetesClientBuilder().build();

    public String deploymentRolloutRestart(String namespace, String deployment) {
        return k8s.apps().deployments().inNamespace(namespace).withName(deployment)
                .rolling()
                .restart()
                .getStatus()
                .toString();
    }

    public String getDeploymentStatus(String namespace, String deployment) {
        return k8s.apps().deployments().inNamespace(namespace).withName(deployment)
                .get()
                .getStatus()
                .toString();
    }


    public List<String> getPodStatus(String namespace, String pod) {

            return k8s.pods().inNamespace("kbadm-space").withName("kbadm-pod").get()
                    .getStatus().getContainerStatuses().stream().map(
                            containerStatus -> {
                                ContainerState state = containerStatus.getState();
                                String containerState = null;
                                if (state.getRunning() != null) {
                                    containerState =  "Running -> Started At: " + state.getRunning().getStartedAt();
                                }
                                if (state.getWaiting() != null) {
                                    containerState =  "Waiting -> " + state.getWaiting().getMessage() + state.getWaiting().getReason();
                                }
                                if (state.getTerminated() != null) {
                                    containerState =  "Terminated -> " + state.getTerminated().getMessage() + state.getTerminated().getReason();
                                }
                                return containerState;
                            }
                    ).collect(Collectors.toList());

    }

    public String getPodDesc(String namespace, String pod) {
        return k8s.pods().inNamespace(namespace).withName(pod).get().toString();
    }

    // public KuberService() throws IOException {
    //     Configuration.setDefaultApiClient(ClientBuilder.defaultClient());
    //     KubernetesClient client = new KubernetesClientBuilder().withConfig(config).build();
//
    // }
//
    // public V1Pod getPodStatu(String namespace, String podName) throws KubectlException {
    //     return Kubectl.get(V1Pod.class)
    //             .namespace(namespace)
    //             .name(podName)
    //             .execute();
    // }
//
    // public String restartPodInNamespaceByName(String namespace, String podName) throws KubectlException {
    //     //todo
    //     Kubectl.rollout().
    //     throw new UnsupportedOperationException("");
    // }
    // public String getAllPodsInNamespace(String namespace) throws KubectlException {
    //     return Kubectl.get(V1Pod.class)
    //             .namespace(namespace)
    //             .execute().stream().map(V1Pod::toString)
    //             .collect(Collectors.joining(","));
    // }

}
