package com.mtsb.k8sadmserver.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class InMemBasicAuthSecurityConfigurer {


    @Value("${kbadm.sec.username}")
    private String username;

    @Value("${kbadm.sec.password}")
    private String password;

    private static final String ROLES = "USER";


    @Bean
    public PasswordEncoder userPasswordEncoder() {
        return new BCryptPasswordEncoder( 4);
    }


    @Bean
    public UserDetailsService userDetailsService(@Autowired PasswordEncoder passwordEncoder) {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User.withUsername(username).password(passwordEncoder.encode(password)).roles(ROLES).build());
        return manager;

    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(
                        (authz) -> authz.anyRequest().authenticated()
                ).httpBasic(withDefaults())
                .csrf().disable();
        return http.build();
    }


}